import React from 'react';

const BrowserList = ({ browsers }) => {
  return (
    <section>
    <article>
      <h2>Browser List</h2>
      <ul>
        {browsers.map((browser, index) => (
          <li key={index}>
            <h3>{browser.name}</h3>
            <img src={browser.logo} alt={`${browser.name} Logo`} />
            <p>{browser.description}</p>
          </li>
        ))}
      </ul>
    </article>
    </section>
  );
};

export default BrowserList;
